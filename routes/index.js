const { raw } = require('express');
var express = require('express');
var router = express.Router();
var fs = require("fs");
const { type } = require('os');
var path = require("path");
var db = require("../bin/mysql");


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'League of legends' });
});

/* GET other characters page. */
router.get('/json-load', function(req, res, next) {
  var rawdata = fs.readFileSync(path.join(__dirname, "../data", "data.json"));
  var data = JSON.parse(rawdata);
  res.render('json', { title: 'JSON loader', data: data });
});

router.post('/json-load', function(req, res, next) {
  var filepath = path.join(__dirname, "../data", "data.json");
  var {image, name, characteristics} = req.body;
  var rawdata = fs.readFileSync(filepath);
  var data = JSON.parse(rawdata);

  data.push({
    name, photo: image, characteristics
  });

  var new_data = JSON.stringify(data);
  fs.writeFileSync(filepath, new_data);
  res.redirect('/json-load');

});

router.get('/database', function(req, res, next) {
  db.query('SELECT * FROM personnage', (err,rows) => {
    if(err) throw err;
  
    console.log('Data received from Db:\n');
    console.log(rows);
    res.render('database', { title: 'Database listing', data: rows });
  });
  
});

router.post('/database', function(req, res, next) {
  var {image, name, characteristics} = req.body;
  var insert_query = `INSERT INTO personnage (name, photo, characteristics) VALUES ('${name}', '${image}', '${characteristics}')`;  

  db.query(insert_query, function (err, result) {  
    if (err) throw err;  
    console.log(`personnage ${name} has ben inserted.`);

    res.redirect('/database');
  });  

  

});

module.exports = router;
