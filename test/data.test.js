const expect = require("chai").expect;
const fs = require("fs");
const path = require("path");

describe("Data Testing", () =>{
    it("list of personnage should return an array", () => {
        var rawdata = fs.readFileSync(path.join(__dirname, "../data", "data.json"));
        var data = JSON.parse(rawdata);
        expect(data).to.be.an("array");
    });
    
});